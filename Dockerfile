FROM golang:1.6
RUN go get github.com/labstack/echo
RUN mkdir /diagram
ADD . /diagram
WORKDIR /diagram/server
RUN pwd
RUN go build && ls
CMD ["./server"]
