// EXAMPLE
//      {"from": "Radius",
//      "to": "API1A",
//      "text": "An HTTP request (POST) is sent to our \"/v1/event\" route.",
//      "category": "OUT",
//      "color": colors.radius}
var links;

(function() {
      var radius = [{"from": "Radius1",
                     "to": "API1A",
                     "text": "An HTTP request (POST) is sent to our \"/v1/event\" route.",
                     "category": "OUT",
                     "color": colors.radius},
                    {"from": "Radius1",
                     "to": "API1B",
                     "text": "An HTTP request (DELETE) is sent to our \"/v1/event\" route.",
                     "category": "OUT",
                     "color": colors.radius},];
      var API = [{"from": "API1A",
                  "to": "API2",
                  "text": "Offers are unbundled",
                  "category": "OUT",
                  "color": colors.API},
                 {"from": "API1B",
                  "to": "API2",
                  "text": "Offers are unbundled",
                  "category": "OUT",
                  "color": colors.API},
                 {"from": "API2",
                  "to": "API3",
                  "text": "",
                  "category": "OUT",
                  "color": colors.API,
                  "routing": go.Link.Normal},
                 {"from": "API3",
                  "to": "API4",
                  "text": "",
                  "category": "OUT",
                  "color": colors.API,
                  "routing": go.Link.Normal},
                 {"from": "API4",
                  "to": "API5A",
                  "label": "No",
                  "labelColor": colors.API},
                 {"from": "API4",
                  "to": "API5B",
                  "label": "Yes",
                  "labelColor": colors.API},
                 {"from": "API5A",
                  "to": "API6",
                  "text": "",
                  "category": "OUT",
                  "color": colors.API,
                  "routing": go.Link.Orthogonal},
                 {"from": "API5B",
                  "to": "API6",
                  "text": "",
                  "category": "OUT",
                  "color": colors.API,
                  "routing": go.Link.Orthogonal},];

       var rightsLogic = [{"from": "RL1",
                           "fromSpot": "LeftCenter",
                           "to": "API6",
                           "toSpot": "RightCenter",
                           "text": "",
                           "category": "IN",
                           "color": colors.rightsLogic},
                          {"from": "RL1",
                           "fromSpot": "MiddleBottom",
                           "to": "RL2",
                           "toSpot": "MiddleTop",
                           "text": "",
                           "category": "OUT",
                           "color": colors.rightsLogic},
                          {"from": "RL2",
                           "fromSpot": "MiddleBottom",
                           "to": "RL3",
                           "toSpot": "MiddleTop",
                           "text": "",
                           "category": "OUT",
                           "color": colors.rightsLogic,
                           "routing": go.Link.Normal},
                          {"from": "RL3",
                           "fromSpot": "LeftCenter",
                           "to": "RL4A",
                           "toSpot": "RightCenter",
                           "text": "",
                           "label": "Yes, > 1",
                           "labelColor": colors.rightsLogic},
                          {"from": "RL3",
                           "fromSpot": "BottomCenter",
                           "to": "RL4B",
                           "toSpot": "MiddleTop",
                           "text": "",
                           "label": "Yes, only 1",
                           "labelColor": colors.rightsLogic},
                          {"from": "RL3",
                           "fromSpot": "RightCenter",
                           "to": "RL4C",
                           "toSpot": "LeftCenter",
                           "text": "",
                           "label": "No",
                           "labelColor": colors.rightsLogic},
                          {"from": "RL4B",
                           "fromSpot": "MiddleBottom",
                           "to": "RL5",
                           "toSpot": "MiddleTop",
                           "category": "OUT",
                           "color": colors.rightsLogic,
                           "text": "",
                           "routing": go.Link.Normal},
                          {"from": "RL5",
                           "fromSpot": "LeftCenter",
                           "to": "RL6A",
                           "toSpot": "RightCenter",
                           "text": "",
                           "label": "No",
                           "labelColor": colors.rightsLogic,
                            "routing": go.Link.Normal},
                          {"from": "RL5",
                           "fromSpot": "RightCenter",
                           "to": "RL6B",
                           "toSpot": "LeftCenter",
                           "text": "",
                           "label": "Yes",
                           "labelColor": colors.rightsLogic},
                          {"from": "RL6B",
                           "fromSpot": "RightCenter",
                           "to": "RL7",
                           "toSpot": "LeftCenter",
                           "text": "",
                           "category": "OUT",
                           "label": "",
                           "color": colors.rightsLogic},
                          {"from": "RL7",
                           "fromSpot": "MiddleBottom",
                           "to": "RL8",
                           "toSpot": "MiddleTop",
                           "text": "",
                           "category": "OUT",
                           "label": "",
                           "routing": go.Link.Normal,
                           "color": colors.rightsLogic},
                          {"from": "RL8",
                           "fromSpot": "RightCenter",
                           "to": "RL9A",
                           "toSpot": "LeftCenter",
                           "text": "",
                           "label": "Yes",
                           "labelColor": colors.rightsLogic,
                           "routing": go.Link.Normal},
                          {"from": "RL8",
                           "fromSpot": "MiddleBottom",
                           "to": "RL9B",
                           "toSpot": "MiddleTop",
                           "text": "",
                           "label": "No",
                           "labelColor": colors.rightsLogic,
                           "routing": go.Link.Normal},
                          {"from": "RL9B",
                           "fromSpot": "MiddleBottom",
                           "to": "RL10",
                           "toSpot": "MiddleTop",
                           "text": "",
                           "category": "OUT",
                           "color": colors.rightsLogic,
                           "label": "",
                           "routing": go.Link.Normal},
                          {"from": "RL10",
                           "fromSpot": "MiddleBottom",
                           "to": "RL11B",
                           "toSpot": "MiddleTop",
                           "text": "",
                           "label": "No",
                           "labelColor": colors.rightsLogic,
                           "routing": go.Link.Normal},
                          {"from": "RL10",
                           "fromSpot": "RightCenter",
                           "to": "RL11A",
                           "toSpot": "LeftCenter",
                           "text": "",
                           "label": "Yes",
                           "labelColor": colors.rightsLogic},
                          {"from": "RL11A",
                           "fromSpot": "RightCenter",
                           "to": "RL12",
                           "toSpot": "LeftCenter",
                           "category": "OUT",
                           "color": colors.rightsLogic,
                           "text": "",
                           "label": ""},
                          {"from": "RL12",
                           "fromSpot": "RightCenter",
                           "to": "RL13A",
                           "toSpot": "LeftCenter",
                           "category": "OUT",
                           "text": "",
                           "label": "No",
                           "labelColor": colors.rightsLogic,
                           "routing": go.Link.Normal},
                          {"from": "RL12",
                           "fromSpot": "MiddleBottom",
                           "to": "RL13B",
                           "toSpot": "MiddleTop",
                           "category": "OUT",
                           "text": "",
                           "label": "Yes",
                           "labelColor": colors.rightsLogic},
                          {"from": "RL13B",
                           "fromSpot": "RightCenter",
                           "to": "RL14A",
                           "toSpot": "LeftCenter",
                           "text": "",
                           "label": "No",
                           "labelColor": colors.rightsLogic,
                           "routing": go.Link.Normal},
                          {"from": "RL13B",
                           "fromSpot": "MiddleBottom",
                           "to": "RL14B",
                           "toSpot": "MiddleTop",
                           "category": "OUT",
                           "text": "",
                           "label": "Yes",
                           "labelColor": colors.rightsLogic},
                          {"from": "RL14B",
                           "fromSpot": "RightCenter",
                           "to": "RL15A",
                           "toSpot": "LeftCenter",
                           "text": "",
                           "label": "No",
                           "labelColor": colors.rightsLogic,
                          "routing": go.Link.Normal},
                          {"from": "RL14B",
                           "fromSpot": "MiddleBottom",
                           "to": "RL15B",
                           "toSpot": "LeftCenter",
                           "text": "",
                           "label": "Yes",
                           "labelColor": colors.rightsLogic},
                          {"from": "RL13A",
                           "fromSpot": "RightCenter",
                           "to": "RL16",
                           "toSpot": "LeftCenter",
                           "text": "",
                           "category": "OUT",
                           "color": colors.rightsLogic},
                          {"from": "RL14A",
                           "fromSpot": "RightCenter",
                           "to": "RL16",
                           "toSpot": "LeftCenter",
                           "text": "",
                           "category": "OUT",
                           "color": colors.rightsLogic},
                          {"from": "RL15A",
                           "fromSpot": "RightCenter",
                           "to": "RL16",
                           "toSpot": "LeftCenter",
                           "text": "",
                           "category": "OUT",
                           "color": colors.rightsLogic},
                          {"from": "RL15B",
                           "fromSpot": "RightCenter",
                           "to": "RL16",
                           "toSpot": "LeftCenter",
                           "text": "",
                           "category": "OUT",
                           "color": colors.rightsLogic},
                           ];
    var agent = [{"from": "Agent1",
                  "fromSpot": "LeftCenter",
                  "to": "RL16",
                  "toSpot": "RightCenter",
                  "text": "",
                  "category": "IN",
                  "color": colors.agent},
                 {"from": "Agent1",
                  "fromSpot": "MiddleBottom",
                  "to": "Agent2",
                  "toSpot": "MiddleTop",
                  "text": "",
                  "category": "OUT",
                  "color": colors.agent},
                 {"from": "Agent2",
                  "fromSpot": "MiddleBottom",
                  "to": "Agent3",
                  "toSpot": "MiddleTop",
                  "text": "",
                  "category": "OUT",
                  "color": colors.agent},
                 {"from": "Agent3",
                  "fromSpot": "MiddleBottom",
                  "to": "Agent4",
                  "toSpot": "MiddleTop",
                  "text": "",
                  "category": "OUT",
                  "color": colors.agent},
                 {"from": "Agent4",
                  "fromSpot": "MiddleBottom",
                  "to": "Agent5",
                  "toSpot": "MiddleTop",
                  "text": "",
                  "category": "OUT",
                  "color": colors.agent},
                 {"from": "Agent5",
                  "fromSpot": "MiddleBottom",
                  "to": "Agent6",
                  "toSpot": "MiddleTop",
                  "text": "",
                  "category": "OUT",
                  "color": colors.agent},
                 {"from": "Radius2",
                  // "fromSpot": "MiddleBottom",
                  "to": "Agent6",
                  // "toSpot": "MiddleTop",
                  "text": "",
                  "category": "IN",
                  "color": colors.radius},];

            links = helpers.combine([radius, API, rightsLogic, agent]);
})()