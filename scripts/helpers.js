var helpers = Object.freeze({combine: function (argsArray) {
                                           var reducer = function(previous, current) {
                                               return previous.concat(current);
                                           }
                                           return argsArray.reduce(reducer)
                                       },
                              loop: function () {
                                        var diagram = processDiagram;
                                        setTimeout(function() {
                                          diagram.links.each(function(link) {
                                              var shape = link.findObject("PIPE");
                                              var direction = diagram.model.getCategoryForLinkData(link.data);
                                              var offset;
                                              if (direction === "OUT") {
                                                  offset = shape.strokeDashOffset - 2;
                                                  shape.strokeDashOffset = (offset <= 0) ? 20 : offset;
                                              }
                                              if (direction === "IN") {
                                                 offset = shape.strokeDashOffset + 2;
                                                  shape.strokeDashOffset = (offset >= 20) ? 0 : offset;
                                              }
                                            });
                                          helpers.loop();
                                        }, 50);
              }});