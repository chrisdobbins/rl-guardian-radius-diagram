function initTemplates($) {
    initLinkTemplate($);
    initNodeTemplate($);
}

function initNodeTemplate($) {
      processDiagram.nodeTemplateMap.add("SwimLane",
      $(go.Node, "Auto",
        new go.Binding("location", "pos", go.Point.parse).makeTwoWay(go.Point.stringify),
        $(go.Shape, "Rectangle",
          {fill: "#626263"},
          new go.Binding("fill", "color")
        ),
        $(go.TextBlock,
          { alignment: go.Spot.Center,
            textAlign: "center",
          margin: 5},
          new go.Binding("text").makeTwoWay())
          ));

      processDiagram.nodeTemplateMap.add("Decision",
      $(go.Node, "Spot",
        new go.Binding("fromSpot", "fromSpot", go.Spot.parse),
        new go.Binding("toSpot", "toSpot", go.Spot.parse),
        new go.Binding("location", "pos", go.Point.parse).makeTwoWay(go.Point.stringify),
        $(go.Panel, "Auto",
          $(go.Shape, 
            {minSize: new go.Size(150, 150),
             maxSize: new go.Size(150, 150)},
            "Diamond",
            new go.Binding("fill", "color")
           ),
            makePort($, "left", go.Spot.Left),
            makePort($, "right", go.Spot.Right),
            makePort($, "bottom", go.Spot.Bottom),
            makePort($, "top", go.Spot.Top),
           $(go.TextBlock,
           { alignment: go.Spot.Center,
             textAlign: "center",
             maxSize: new go.Size(75, 75),
             margin: 5,
             wrap: go.TextBlock.WrapFit},
           new go.Binding("text").makeTwoWay())
      )));

      processDiagram.nodeTemplateMap.add("Step",
      $(go.Node, "Spot",
        new go.Binding("location", "pos", go.Point.parse).makeTwoWay(go.Point.stringify),
        $(go.Panel, "Auto",
          $(go.Shape, 
            "Rectangle",
            new go.Binding("fill", "color")),
            makePort($, "left", go.Spot.Left),
            makePort($, "right", go.Spot.Right),
            makePort($, "bottom", go.Spot.Bottom),
            makePort($, "top", go.Spot.Top),
          $(go.TextBlock,
          { alignment: go.Spot.Center, 
            textAlign: "center",
            margin: 7.5,
            wrap: go.TextBlock.WrapFit,
            minSize: new go.Size(150, 50),
            maxSize: new go.Size(150, 85) },
             new go.Binding("text"))
        )));
    
}

function initLinkTemplate($) {
  processDiagram.linkTemplate =
      $(go.Link,
         { routing: go.Link.AvoidsNodes },
        new go.Binding("fromSpot", "fromSpot", go.Spot.parse),
        new go.Binding("toSpot", "toSpot", go.Spot.parse),
        new go.Binding("routing"),
        $(go.Shape, 
          { isPanelMain: true, 
            stroke: "black", 
            strokeWidth: 7 }),
        $(go.Shape, 
          { isPanelMain: true, 
            strokeWidth: 5 , 
            stroke: "black"}),
        $(go.Shape, 
        { isPanelMain: true, 
          strokeWidth: 7, 
          name: "PIPE", 
          strokeDashArray: [10, 10]}, 
        new go.Binding("stroke", "color")),
        $(go.Panel, "Auto", 
          $(go.Shape, "Rectangle", {fill: "black"}),
        $(go.TextBlock,
          {stroke: "white"},
          new go.Binding("text", "label"),
          new go.Binding("stroke", "labelColor"))),
        $(go.Shape, 
          { toArrow: "Standard",
            stroke: "black",
            strokeWidth: 2,
            scale: 2.2 },
          new go.Binding("fill", "color")),
          {toolTip: $(go.Adornment, 
                      "Auto",
                      $(go.Shape, 
                        new go.Binding("fill", "color")), 
                      $(go.TextBlock,
                        { margin: 4 },
                        new go.Binding("text", "text"))
                      )}
          );
}

function makePort($, name, spot) {
      // the port is basically just a small circle that has a white stroke when it is made visible
      return $(go.Shape, "Circle",
               {
                  fill: "transparent",
                  stroke: null,  // this is changed to "white" in the showPorts function
                  desiredSize: new go.Size(8, 8),
                  alignment: spot, alignmentFocus: spot,  // align the port on the main Shape
                  portId: name,  // declare this object to be a "port"
                  fromSpot: spot, toSpot: spot
               });
    }