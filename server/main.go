package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/labstack/echo"
)

var rootDir = os.Getenv("ROOT_DIR")
var scriptsDir = os.Getenv("SCRIPTS_DIR")
var fullScriptsDir = fmt.Sprintf("%s/%s", rootDir, scriptsDir)
var port = os.Getenv("PORT")

func main() {
	router := echo.New()
	router.Static("/", rootDir)
	router.File("/processflow", fmt.Sprintf("%s/processflow.html", rootDir))
	files, _ := ioutil.ReadDir(scriptsDir)
	for i := 0; i < len(files); i++ {
		fullFileName := fmt.Sprintf("%s/%s", fullScriptsDir, files[i].Name())
		requestedFile := fmt.Sprintf("%s/%s", scriptsDir, files[i].Name())
		router.File(requestedFile, fullFileName)
	}
	router.Start(fmt.Sprintf(":%s", port))
}
